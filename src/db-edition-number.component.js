/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition-number')
    .component('bitcraftDbEditionNumber', {
        templateUrl: './js/db-edition-checkboxes-number/db-edition-checkboxes-number.template.html', // this line will be replaced
        bindings: {
            value: '=',
            edit: '<'
        }
    });
